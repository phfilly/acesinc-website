<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', ['as' => 'login', 'uses' => 'Api\UserController@login']);
Route::get('login', ['as' => 'login', 'uses' => 'Api\UserController@login']);
Route::post('register', 'Api\UserController@register');

Route::group(['middleware' => 'auth:api'], function(){
	Route::post('details', 'Api\UserController@details');
});

Route::get('/pages', 'HomePageController@getPages')->middleware('auth:api');

Route::get('/menu', 'HomePageController@getMenu')->middleware('auth:api');