 <!-- Demo Section -->
<section id="demo" class="content-section text-center">
    <div class="container demo-block">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="white-text">Book a demo</h2>
                <p class="white-text">Please fill in your contact details below to book a demo</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <form method='post' action='/dashboard'>

                    {{ csrf_field() }}
                    <div class="input-group demo-userinput">
                        <input id="txtUser" type="text" class="form-control" name="Name" placeholder="Name" required>
                    </div>
                    
                    <div class="input-group demo-userinput">
                        <input id="txtUser" type="email" class="form-control" name="email" placeholder="Email" required>
                    </div>

                    <div class="input-group demo-userinput">
                        <input  id="txtPassword" type="text" class="form-control" name="phone" placeholder="Phone" required> 
                    </div>

                    <div class="input-group demo-userinput">
                        <textarea class="form-control" name="message" placeholder="Message"></textarea> 
                    </div>
                    <br>
                    <button class="btn btn-primary btn-block btn-demo" type="submit">
                        <i class="fa fa-envelope" aria-hidden="true"></i> Book
                    </button>

                </form>
            </div>
            <div class="col-md-2"></div>		
        </div>
    </div>
</section>