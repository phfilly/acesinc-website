 <!-- About Section -->
<section id="about" class="content-section text-center">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 mx-auto">
                <h2 class="white-text">About With Nova</h2>
                {!! $data[0]->body !!}
            </div>
        </div>
    </div>
</section>
<div class="page-divider"></div>
