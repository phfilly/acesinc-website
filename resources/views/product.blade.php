 <!-- Product Section -->
<section id="product" class="content-section text-center">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2>Our Product</h2>
            </div>
        </div>
        <br>
        <div class="row">
                <div class="col-md-4 product-block">
                    <img src="images/certificate.png" class="product-image"/>
                    <h5 class="product-heading">Customer Experience</h5>
                    <p>WithNova gives organizations complete control over their customer experience programs. Respond to customer needs, analyze trends, and make changes in real time. WithNova is easy to use, extremely flexible, and automates processes that drive action.</p>
                </div>
                <div class="col-md-4 product-block">
                    <img src="images/graph.png" class="product-image" style="border-radius: 50%"/>
                    <h5 class="product-heading">Employee Insights</h5>
                    <p>Collect and act on employee feedback in real time. Organizations can now manage all their employee insights in one place. From employee engagement programs to 360 performance reviews</p>
                </div>
                <div class="col-md-4 product-block">
                    <img src="images/analytics.png" class="product-image"/>
                    <h5 class="product-heading">Market Research</h5>
                    <p>For when you need an insight platform that’s fast and flexible enough to put the customer’s voice into every step of the development process</p>
                </div>
            </div>
        </div>
    </div>
</section>