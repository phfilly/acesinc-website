 <!-- Contact Section -->
<section id="contact" class="content-section text-center" style="background-color:#000; border-top: 15px solid #007bff">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="white-text">Get in touch</h2>
                <p class="white-text">We would love to hear from you</p>
            </div>
        </div>
        <br><br>
        <div class="row">
            <div class="col-md-4">
                <img src="images/emailicon.png" class="touch-icons">
                <a href="mailto:marnus@withnova.co.za">marnus@withnova.co.za</a>
            </div>
            <div class="col-md-4">
                <img src="images/phoneicon.png" class="touch-icons">
                <a href="tel:0825606052">082 560 6052</a>
            </div>
            <div class="col-md-4">
                <img src="images/locationicon.png" class="touch-icons">
                <a href="#">16 Troupant Street Stellenbosch Cape Town</a>
            </div>
        </div>
        <!-- <img src="images/logo.png" style="max-width: 60px"/>-->
    </div>
</section>