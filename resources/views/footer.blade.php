<footer>
    <div class="container text-center">
        <div class="row">
            <div class="col-lg-12">
                <a href='https://www.facebook.com/Withnova/' target="_blank" class="social-icons">
                    <i class="fa fa-facebook-square fa-3" aria-hidden="true"></i>
                </a>
                <a href='https://twitter.com/with_nova' target="_blank" class="social-icons">
                    <i class="fa fa-twitter-square fa-3" aria-hidden="true"></i>
                </a>
                <a href='http://www.linkedin.com/company/with-nova?trk=top_nav_home' target="_blank" class="social-icons">
                    <i class="fa fa-linkedin-square fa-3" aria-hidden="true"></i>
                </a>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-lg-12">
                <p>Copyright &copy; With Nova 2017</p>
            </div>
        </div>
    </div>
</footer>

<!-- Bootstrap core JavaScript -->
<script src="js/jquery.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>

<!-- Plugin JavaScript -->
<script src="js/jquery.easing.js"></script>

<!-- Custom scripts for this template -->
<script src="js/grayscale.js"></script>