 <!-- Consulting Section -->
<section id="consulting" class="content-section text-center" style="background-color:#1c1f23;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="consulting-header">Consulting</h2>
            </div>
        </div>
        <br><br>
        <div class="row">
            <div class="col-md-4">
                <img src="images/megaphone.png" class="touch-icons">
                <br>
                <h5 class="consulting-header">Marketing</h5>
            </div>
            <div class="col-md-4">
                <img src="images/strategy.png" class="touch-icons">
                <br>
                <h5 class="consulting-header">Strategy</h5>
            </div>
            <div class="col-md-4">
                <img src="images/group.png" class="touch-icons">
                <br>
                <h5 class="consulting-header">Human Resources</h5>
            </div>
        </div>
    </div>
</section>