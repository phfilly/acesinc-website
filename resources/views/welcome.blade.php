<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Aces Inc</title>

        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link href="css/bootstrap.css" rel="stylesheet">
        <link href="css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="css/grayscale.css" rel="stylesheet">
        <link rel="icon" href="images/favicon.png">
        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100%;
                margin: 0;
                background-image: url('../public/images/background.jpg');
            }

            h1 {
                color: #F7153D;
                font-size: 65px;
                font-weight: bold;
            }

            .center {
                display: flex;
                align-items: center;
                justify-content: center;
                height: 100vh;
            }

            .item {
                max-width: 50%;
                flex: 1;
                text-align: center;
            }

        </style>
    </head>
    <body id="page-top">
        <div class='center'>
            <div class='item'>
                <h1>ACES INC</h1>
                <h1 style='color: #fff;'>COMING SOON!</h1>
            </div>
        </div>
    </body>
</html>
