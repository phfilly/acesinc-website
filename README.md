# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

For development shit

### How do I get set up? ###

- npm install
- composer install
- php artisan serve

### How to generate an Auth token ###

- Be sure there is a user already in the users table
- Do a POST request to /api/login with the users email and password
- You will receive your token
- Use this token for your future requests
    i.e If you do a GET request add in the 'Authorization' key followed by the value 'Bearer YOUR TOKEN HERE'.

P.S there is a space between Bearer and YOUR TOKEN HERE
