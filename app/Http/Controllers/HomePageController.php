<?php

namespace App\Http\Controllers;

use App\Pages;
use App\MenuItems;
use Illuminate\Http\Request;
use Response;

class HomePageController extends Controller
{
    //
    public function homepage()
    {
        $pages = Pages::where('status', 'ACTIVE')
               ->get();

        return view('welcome', ['data' => $pages]);
    }

    public function getPages()
    {
        $pages = Pages::where('status', 'ACTIVE')
               ->get();

        return Response::json(['pages' => $pages]);
    }

    public function getMenu()
    {
        $pages = MenuItems::where('id', '<>', '')
               ->get();

        return Response::json(['menu' => $pages]);
    }
}
